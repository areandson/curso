console.log(`Meu servidor`);
//const funcoes = require('./funcoes-require');

//import {} from './funcoes-import.js';  

//console.log(diminuir);
/*console.log(funcoes.soma(1,2));
console.log(funcoes.diminuir(2,1));
*/

const pessoa = {
    nome : "areandson",
    idade : 38
}



let { nome, idade } = pessoa;
idade ++;
console.log(nome, idade);

const gabriel = {...pessoa};

gabriel.nome = "gabriel";

console.log(gabriel);

console.log(pessoa);

//funcao callback


function calcular(numero1, numero2,funcaoDeCalculo){
    const valor = funcaoDeCalculo(numero1,numero2);
    console.log(`O Calculo deu ${valor}`);
}

function soma(a,b){
    return a + b
}

function diminuir(a,b) {
    return a - b
}

calcular(1,2,soma);
calcular(1,2,diminuir);
calcular(10,2,(x,y)=>{
   return x/y;
})

