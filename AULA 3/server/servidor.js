console.log(`Meu servidor`);
import useExpress from 'express';
import bodyParser from 'body-parser';

// parse application/x-www-form-urlencoded

const app = useExpress()
const port = 3000
app.use(bodyParser.urlencoded({ extended: false }))

app.get('/', (req, res) => {
  res.send('Hello World!')
})
app.get('/gabriel', (req, res) => {
    
    const dados = {
        nome : "areandson",
        idade : 38
    }

    res.send(dados)
  })

app.post('/veiculo', (req, resposta) => {
 
 
  resposta.status = 200;
  resposta.send({sucesso: true,
  ...req.body
   });

});  
  

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})