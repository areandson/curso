import { Sequelize } from 'sequelize';

const config = {
    dbname : 'aula',
    username : 'postgres',
    password : 'postgres',
    host : 'localhost',
    port : 5432 
 };

let orm = null;

function createOrm(){
    orm = new Sequelize(`postgres://${config.username}:${config.password}@${config.host}:${config.port}/${config.dbname}`); 
    orm.sync();
     return orm;
}

function getOrm(){
    if (!orm) createOrm()
    return orm;
}

export function testConnection(){
    const test = getOrm()
    test.authenticate()
    .then(()=>{
        console.log('Conectado ao banco de dados com sucesso!');
    })
    .catch((erro)=>{
       console.log('Erro ao conectar ao banco: ' + erro);
    });
}


 export default getOrm;