import express  from 'express'
//import bodyParser from 'body-parser';
import {testConnection} from './orm.js';
import Item from './modelos/itemModel.js';
import cors from 'cors';

console.log('Iniciando Servidor');
testConnection();

const app = express();
app.use(cors())
//app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
const port = 3000;


app.get('/',(requisicao, resposta) => {
  resposta.status = 500;
  resposta.send('<html><body><h1>Path Invalido</h1></body></html>');
});

app.get('/items',(requisicao, resposta) => {
    Item.findAll().then((items) => {
       resposta.status = 200;
       resposta.send(items);
   }).catch((err) => {
       resposta.status = 400;
       resposta.send({sucesso: false, mensagem:  `Erro ao buscar items!: ${err}`});
   }) 
  });

app.get('/item/:id',(requisicao, resposta) => {
    Item.findOne({ where: { id: requisicao.params.id } }).then((items) => {
        resposta.status = 200;
        resposta.send({sucesso: true, dados:items });
    }).catch((err) => {
        resposta.status = 400;
        resposta.send({sucesso: false, mensagem:  `Erro ao buscar item!: ${err}`});
    }) 
  });

  app.delete('/item/:id',(requisicao, resposta) => {
    Item.findByPk(requisicao.params.id)
    .then((item) => {
        return item.destroy()
    }).then(()=>{
        resposta.status = 200;
        resposta.send({sucesso: true, mensagem:  'Excluido com sucesso'});

    }).catch((err) => {
        resposta.status = 400;
        resposta.send({sucesso: false, mensagem:  `Erro ao excluir item!: ${err}`});
    }) 
  });

  app.post('/item',async(requisicao, resposta) => {
    

    const dados  = requisicao.body;

    console.log(dados);
    console.log(requisicao.data);
    

    try
    {
     const item = await Item.findByPk(dados.id);
     if (item){
         await item.update(dados); 
    }else {
     await Item.create({
         id : dados.id, 
         nome : dados.nome, 
         valor : dados.valor
           });
    }
    
        resposta.status = 200;
        resposta.send({sucesso: true, mensagem:  `Item  ${dados.nome} cadastrado com sucesso!`});

    } catch(err) {
        resposta.status = 400;
        resposta.send({sucesso: false, mensagem:  `Erro ao salvar item! : ${err}`});
    } 
   
});

app.listen(port,()=>{
   console.log('Servidor online!');
});

