import { DataTypes } from 'sequelize';
import getOrm from '../orm.js';

 const Item = getOrm().define('Item',{ 
    id : {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    nome : {
        type: DataTypes.STRING,
        allowNull: false
    },
    valor : {
        type: DataTypes.DOUBLE,
    }

 },
 {
    tableName: 'item'
  }
 )

 export default Item;